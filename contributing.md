# Contribution Guidelines

Thank you for deciding to contribute to this project :)  
Please follow these guidelines when implementing your code.

[[_TOC_]]

## Change workflow

The `main` branch contains the latest version of the code. This branch must be
stable and working at any time. To ensure this CI pipelines are used.

The workflow is the following:
1. create a branch on the main repository with an explicit name
1. create a merge request from your branch against the `main` branch
1. a pipeline is created each time you push commits in a merge request but it
   will not start automatically: the user may start it. Since a merge request
   cannot be merged until the associated pipeline passed, start the `blocked
   pipeline` associated with the latest commit in your merge request when your
   change is ready.
1. if the pipeline passed, the merge request may be merged by one of the
   maintainers. Note that the preferred option is to squash commits.

Note: more information about the pipeline is available in the
[CI file](.gitlab-ci.yml).

## Design

The add-on is based on the following python modules:

| Name | Description |
| ------ | ------ |
| main.py | Entry point of the add-on. |
| service.py | Service responsible for downloading torrent files in the background. |
| resources/lib/addon.py | It handles the routing and the interaction between the other modules. |
| resources/lib/peertube.py | Responsible for interacting with PeerTube. |
| resources/lib/kodi_utils.py | Provides utility functions to interact easily with Kodi. |

### main.py

The file `peertube.py` is currently being redesigned into the `main` module.

This module must be as short as possible (15 effective lines of code maximum)
to comply with Kodi add-on development best practices (checked by the
[Kodi add-on checker](https://github.com/xbmc/addon-check)).

### service.py

This module is being redesigned currently.

This module must be as short as possible (15 effective lines of code maximum)
to comply with Kodi add-on development best practices (checked by the
[Kodi add-on checker](https://github.com/xbmc/addon-check)).

### addon.py

This module does not exist yet.

### peertube.py

This file contains:
* the class PeerTube which provides simple method to send REST APIs to a
  PeerTube instance
* the function `list_instances` which lists the PeerTube instances from
  joinpeertube.org. The URL of the API used by this function and the structure
  of the response in case of errors is different than the other PeerTube APIs
  (which are sent to a specific instance) so it made sense to have it as a
  dedicated function. If more instance-related API are used in the future, a
  class could be created.

### kodi_utils.py

This module only contains functions (no classes) as no common data between them
was identified.

The functions must be sorted alphabetically to make the maintenance easier.

## Coding style

Here are the rules to follow when modifying the code:
* document the usage of functions following [Sphinx
  format](https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#python-signatures)
* use double quotes instead of single quotes when defining strings (to avoid
  escaping apostrophes which are common characters in English and other
  languages)
* follow [PEP 8](https://www.python.org/dev/peps/pep-0008/) conventions. The
  compliance is checked in the `quality` job (more details are available in the
  [CI file](.gitlab-ci.yml)). Pylint can also be run locally with the
  following commands:

```python
python3 -m pip install -r misc/python_requirements.txt
python3 -m pylint --rcfile=misc/pylint-rcfile.txt
```

Note: pylint is run with python3 to have latest features even though the add-on
only supports Kodi v18 Leia (which uses python2)

## How to release a new version of this add-on

These steps should be followed only by maintainers.

1. Create a release branch whose name follows this format:
   `release/<release_name>`
2. On this branch don't commit any new feature. Only commit changes related to
   the release process like:
    - a bump of the add-on version in `addon.xml` (note that the version
      numbering must follow the [semantic versioning](https://semver.org/))
    - the update of the changelog in the `news` tag in `addon.xml` (using
      Markdown syntax since it will be re-used automatically in the release
      notes)
3. Merge the merge request (maintainers only)
4. A new pipeline with the job `create-release` will be created: run the job
   manually since it should be `blocked` (maintainers only)
5. The new release will be available on the releases page.
